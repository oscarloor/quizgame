//
//  tipoDeJuego.swift
//  QuizGame
//
//  Created by Oscar on 17/8/17.
//  Copyright © 2017 Oscar. All rights reserved.
//

import UIKit

class tipoDeJuego {
    //MARK: Properties
    
    var nombre: String

    
    //MARK: Initialization
    
    init?(nombre: String) {
        
        // Initialization should fail if there is no name or if the rating is negative.
        if nombre.isEmpty{
            return nil
        }
        // Initialize stored properties.
        self.nombre = nombre
        
        
    }
    
    
    
}
