//
//  ViewController.swift
//  QuizGame
//
//  Created by Oscar on 16/8/17.
//  Copyright © 2017 Oscar. All rights reserved.
//

import UIKit
import os.log

class ViewController: UIViewController {
var tipos = [tipoDeJuego]()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
                
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        
        //Llamo al controlador de la pantalla destino
        guard let tipoController = segue.destination as? tipoController else {
            fatalError("Unexpected destination: \(segue.destination)")
        }
        
        switch(segue.identifier ?? "") {
            
        case "unJugadorSegue":
            print("un jugador")
            os_log("Adding a new meal.", log: OSLog.default, type: .debug)
            tipoController.jugadoresSeleccionados = "Uno"
            
        case "dosJugadoresSegue":
            print("dos jugadores")
            
            
            
            //Obtengo el sender que activo el segue
            /*
            guard let celdaSeleccionada = sender as? celdaPropiaTableViewCell else {
                fatalError("Unexpected sender: \(sender)")
            }*/
            
            
            tipoController.jugadoresSeleccionados = "Dos"
            
            
        default:
            fatalError("Unexpected Segue Identifier; \(segue.identifier)")
        }
    }

}

