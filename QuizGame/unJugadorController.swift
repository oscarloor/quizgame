//
//  unJugadorController.swift
//  QuizGame
//
//  Created by Oscar on 16/8/17.
//  Copyright © 2017 Oscar. All rights reserved.
//

import UIKit

class unJugadorController: UIViewController {
    
    @IBOutlet weak var tiempo: UILabel!
    
    @IBOutlet weak var puntuacion: UILabel!
    
    
    @IBOutlet weak var pregunta: UITextView!
    
    
    @IBOutlet weak var btnUno: UIButton!
    
    
    @IBOutlet weak var btnDos: UIButton!
    
    
    @IBOutlet weak var btnTres: UIButton!
    
    
    @IBOutlet weak var btnCuatro: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        tiempo.text = "88s"
        puntuacion.text = "1001"
        pregunta.text = "Otra pregunta"
        btnUno.setTitle("Opción 1", for: .normal)
        btnDos.setTitle("Opción 2", for: .normal)
        btnTres.setTitle("Opción 3", for: .normal)
        btnCuatro.setTitle("Opción 4", for: .normal)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
