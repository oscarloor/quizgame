//
//  tipoController.swift
//  QuizGame
//
//  Created by Oscar on 17/8/17.
//  Copyright © 2017 Oscar. All rights reserved.
//

import UIKit
import os.log
import FirebaseDatabase

class tipoController: UIViewController{
    
    //MARK: Properties
    
    @IBOutlet weak var tablaTipo: UITableView!
    var jugadoresSeleccionados: String?
    var tipos = [tipoDeJuego]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Juego seleccionado")
        print(jugadoresSeleccionados!)
        var ref: DatabaseReference!
        
        ref = Database.database().reference(withPath: "tipo");
        
        ref.observe(DataEventType.value, with: { (snapshot) in
            print("Snapshot es")
            //print(snapshot)
            print("Valores")
            
            for childSnap in  snapshot.children.allObjects {
                let snap = childSnap as! DataSnapshot
                print(snap.key)
                
                var tipoCreacion = tipoDeJuego(nombre: snap.key)
                
                self.tipos.append(tipoCreacion!)
                
                
                //let property = snap.value!["property"] as! NSString
            }
            
            //Imprime los objetos
            print(self.tipos)
            
            //Necesito sacar el valor del nombre de cada objeto y agregarlo a un arreglo
            /*
            var nombreTiposArreglo = [String]()
            for tipo in self.tipos {
                nombreTiposArreglo.append(tipo.nombre)
            }
            print(nombreTiposArreglo)*/
            
            self.tablaTipo.reloadData()
            
            /*
             for(DataSnapshot snapshot2: snapshot.getChildren()){
             Log.d(TAG, snapshot2.getKey();
             Log.d(TAG, snapshot2.getValue();
             
             //Do your operations here
             }
             
             for rest in snapshot.children.allObjects as! [DataSnapshot] {
             print("Datos ")
             
             
             guard let restDict = rest.value as? [String: Any] else { continue }
             var nombre = rest.getChildren().getKey() as? String
             print(nombre)
             /*
             var nombre = restDict["nombre"] as? String
             var imagen = restDict["imagen"] as? String
             print(nombre)
             print(imagen)
             
             let url = URL(string: imagen!)
             let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
             
             
             var pokemonCreacion = Pokemon(nombre: nombre!, imagenPrincipal: UIImage(data: data!), urlImagen: imagen!)
             self.pokemones.append(pokemonCreacion!)*/
             
             }*/
            
            //DEscomentar self.tableView.reloadData()
            
            
            
            // ...
        })

        /*
         
         refHandle = postRef.observe(DataEventType.value, with: { (snapshot) in
         print(snapshot)
         let postDict = snapshot.value as? [String : AnyObject] ?? [:]
         // ...
         })
         */
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        print("Entro en table view")
        //cargarEjemploDePokemones()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    

    
}

extension tipoController: UITableViewDataSource, UITableViewDelegate {
    // MARK: - Table view data source
    
     func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return tipos.count
    }
    
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Configure the cell...
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cellIdentifier = "celdaPropiaTableViewCell"
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? celdaPropiaTableViewCell  else {
            fatalError("The dequeued cell is not an instance of MealTableViewCell.")
        }
        // Fetches the appropriate meal for the data source layout.
        let tipoJuego = tipos[indexPath.row]
        cell.btnCelda.setTitle(tipoJuego.nombre, for: .normal)
        
        
        
        return cell
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    //MARK: Private Methods
}
